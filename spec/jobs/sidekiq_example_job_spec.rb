require 'rails_helper'

RSpec.describe SidekiqExampleJob, type: :job do
  describe "Sidekiq::testing.inline!" do

    def saved_value
      File.read(SidekiqExampleJob::TEST_FILE)
    end

    before do
      ActiveJob::Base.queue_adapter = :sidekiq
      require 'sidekiq/testing'
      Sidekiq::Testing.inline!
    end

    it "should perform_now synchronously" do
      value = 'perform_now'
      SidekiqExampleJob.perform_now(value)
      expect(saved_value).to eq(value)
    end

    it "should perform_later synchonously" do
      value = 'perform_later'
      SidekiqExampleJob.perform_later(value)
      expect(saved_value).to eq(value)
    end
  end
end
