class SidekiqExampleJob < ApplicationJob
  queue_as :default

  TEST_FILE = File.join(Rails.root, 'sidekiq_saved_value')

  def perform(value)
    File.write(TEST_FILE, value)
  end
end
